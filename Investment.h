#include <iostream>

using namespace std;

class Investment
{
	
	
         private:  //properties
         long double value;
         float interest_rate; 
         


         public:  //methods
        
         Investment(long double, float); //default constructor 
         
         //set and get function for value
          void setvalue(long double);
          long double getvalue();
          
          //set and get function for interest rate
          void setinterest_rate(float);
          float getinterest_rate();
          
          //add interest
          void addinterest_rate();

 };

