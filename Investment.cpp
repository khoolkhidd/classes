#include "Investment.h"
#include <iostream>
#include <string>
#include <cmath>



using namespace std;

Investment::Investment(long double val, float rate) //construtor
{
	value = val;
	interest_rate = rate;
}

//set value
 void Investment::setvalue(long double val)
 {
  
   value = val;
 }

//return value
 long double Investment::getvalue()
 {
 return value;
 }
 
//set interest rate 
 void Investment::setinterest_rate(float rate)
 {
	 interest_rate = rate;
 }
 
 //return interest rate
 float Investment::getinterest_rate()
 {
	 return interest_rate;
	 
 }
 
 //add interest rate to value
 void Investment::addinterest_rate()
   {
	 value = value + (value * interest_rate);
   }
 
 
 
 int main()
 {
   
    Investment inv = Investment(100.00, 0.083); 
	int months;
	
	
	while (inv.getvalue() < 1000000)
	{
		for (int i=0; i < 12; i++)
		{
			inv.setvalue(inv.getvalue() + 250);
		}
		
		   inv.addinterest_rate();
		    months++;
     }
 
     cout << " It would take "  << months << " months to reach $1,000,000 \n" ;
 
return 0;
 
}
